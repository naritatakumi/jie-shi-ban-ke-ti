package bulletin_board.beans;

import java.io.Serializable;
import java.util.Date;

public class ViewComment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int comment_id;
	private String text;
    private int contribbutions_id;
    private int user_id;
    private Date created_date;
    private String user_name;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getContribbutions_id() {
		return contribbutions_id;
	}
	public void setContribbutions_id(int contribbutions_id) {
		this.contribbutions_id = contribbutions_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getComment_id() {
		return comment_id;
	}
	public void setComment_id(int comment_id) {
		this.comment_id = comment_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

}
