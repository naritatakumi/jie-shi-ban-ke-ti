package bulletin_board.beans;

import java.io.Serializable;

public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	private int department_position_code;
	private String department_position_name;

	public int getDepartment_position_code() {
		return department_position_code;
	}
	public void setDepartment_position_code(int department_position_code) {
		this.department_position_code = department_position_code;
	}
	public String getDepartment_position_name() {
		return department_position_name;
	}
	public void setDepartment_position_name(String department_position_name) {
		this.department_position_name = department_position_name;
	}


}
