package bulletin_board.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID =1L;

	private int user_id;
	private String login_id;
	private String password;
	private String user_name;
	private int branch_code;
	private int department_position_code;
	private int account_management;
	private Date created_date;
	private Date updated_date;
	private String password_confirmation;

	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(int branch_code) {
		this.branch_code = branch_code;
	}
	public int getDepartment_position_code() {
		return department_position_code;
	}
	public void setDepartment_position_code(int department_position_code) {
		this.department_position_code = department_position_code;
	}
	public int getAccount_management() {
		return account_management;
	}
	public void setAccount_management(int account_management) {
		this.account_management = account_management;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}
	public String getPassword_confirmation() {
		return password_confirmation;
	}
	public void setPassword_confirmation(String password_confirmation) {
		this.password_confirmation = password_confirmation;
	}

}
