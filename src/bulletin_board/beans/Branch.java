package bulletin_board.beans;

import java.io.Serializable;

public class Branch implements Serializable {
	private static final long serialVersionUID = 1L;

    private int branch_code;
    private String branch_name;

	public int getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(int branch_code) {
		this.branch_code = branch_code;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}

}
