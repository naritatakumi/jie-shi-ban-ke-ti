package bulletin_board.beans;

import java.io.Serializable;
import java.util.Date;

public class View implements Serializable {
	private static final long serialVersionUID = 1L;

    private String subject;
    private int id;
    private String text;
    private String category;
    private String user_name;
    private String user_id;
    private Date created_date;

    private String firstDate;
    private String finalDate;
    private String serchCategory;

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstDate() {
		return firstDate;
	}
	public void setFirstDate(String firstDate) {
		this.firstDate = firstDate;
	}
	public String getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}
	public String getSerchCategory() {
		return serchCategory;
	}
	public void setSerchCategory(String serchCategory) {
		this.serchCategory = serchCategory;
	}

}
