package bulletin_board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulletin_board.beans.User;

@WebFilter(urlPatterns = { "/management","/editing","/signup" })
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if (loginUser == null) {
            ((HttpServletResponse) response).sendRedirect("./");
		} else if (loginUser.getDepartment_position_code() != 1) {
			List<String> messages = new ArrayList<String>();
            messages.add("権限がありません");
            session.setAttribute("errorMessages", messages);
            ((HttpServletResponse) response).sendRedirect("./");
		} else {
			chain.doFilter(request, response);
		}

		return;
	}

	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	@Override
	public void destroy() {

	}

}
