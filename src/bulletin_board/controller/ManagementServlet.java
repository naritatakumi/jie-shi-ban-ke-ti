package bulletin_board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulletin_board.beans.Management;
import bulletin_board.beans.User;
import bulletin_board.service.ManagementService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		User login = (User) session.getAttribute("loginUser");
        List<Management> management = new ManagementService().getManagement();

        request.setAttribute("managements", management);
        request.setAttribute("logins", login);

		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}

}
