package bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.Branch;
import bulletin_board.beans.Position;
import bulletin_board.beans.User;
import bulletin_board.service.EditingService;
import bulletin_board.service.UserService;

@WebServlet(urlPatterns = { "/editing" })
public class EditingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		EditingService editingService = new EditingService();

		if ((!isNumber(request.getParameter("user_id")))
				|| (!editingService.getUserValid(request.getParameter("user_id")))) {
			HttpSession session = request.getSession();
			List<String> messages = new ArrayList<String>();
			messages.add("不正なURLです");
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("management");
		} else {
			HttpSession session = ((HttpServletRequest) request).getSession();

			User user = new User();
			user.setUser_id(Integer.parseInt(request.getParameter("user_id")));

			User login = (User) session.getAttribute("loginUser");
			User editing = editingService.getUserEditing(user);
			List<Branch> branchName = editingService.getBranchName();
			List<Position> positionName = editingService.getPositionName();

			request.setAttribute("loginUser", login);
			request.setAttribute("editings", editing);
			request.setAttribute("branchNames", branchName);
			request.setAttribute("positionNames", positionName);

			request.getRequestDispatcher("/editing.jsp").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLogin_id(request.getParameter("login_id"));
			if (!StringUtils.isBlank(request.getParameter("password"))) {
				user.setPassword(request.getParameter("password"));
			}
			user.setUser_name(request.getParameter("user_name"));
			user.setBranch_code(Integer.parseInt(request.getParameter("branch_code")));
			user.setDepartment_position_code(Integer.parseInt(request.getParameter("department_position_code")));
			user.setUser_id(Integer.parseInt(request.getParameter("user_id")));

			new EditingService().edit(user);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);

			User reEditUser = new User();
			reEditUser.setLogin_id(request.getParameter("login_id"));
			reEditUser.setUser_name(request.getParameter("user_name"));
			reEditUser.setBranch_code(Integer.parseInt(request.getParameter("branch_code")));
			reEditUser.setDepartment_position_code(Integer.parseInt(request.getParameter("department_position_code")));
			reEditUser.setUser_id(Integer.parseInt(request.getParameter("user_id")));
			request.setAttribute("editings", reEditUser);

			EditingService editingService = new EditingService();
			List<Branch> branchName = editingService.getBranchName();
			List<Position> positionName = editingService.getPositionName();

			request.setAttribute("branchNames", branchName);
			request.setAttribute("positionNames", positionName);

			request.getRequestDispatcher("/editing.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String user_name = request.getParameter("user_name");
		String branch_code = request.getParameter("branch_code");
		String department_position_code = request.getParameter("department_position_code");
		String password_confirmation = request.getParameter("password_confirmation");
		String user_id = request.getParameter("user_id");

		// ログインIDのエラーメッセージ
		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");
		} else {
			if ((login_id.length() < 6) || (login_id.length() > 20)) {
				messages.add("ログインIDは6文字以上20文字以下です");
			}
			if (!login_id.matches("^[0-9a-zA-Z]+$")) {
				messages.add("ログインIDは半角英数字です");
			}
		}

		// ログインIDの使用済みエラー
		UserService userService = new UserService();
		EditingService editingService = new EditingService();
		if (userService.getLoginIdValid(login_id)) {
			User user = new User();
			user.setUser_id(Integer.parseInt(user_id));
			User editing = editingService.getUserEditing(user);

			if (!editing.getLogin_id().equals(login_id)) {
				messages.add("入力されたログインIDはすでに使用されています");
			}
		}

		// パスワードのエラーメッセージ
		if (!StringUtils.isBlank(password)) {
			if ((password.length() < 6) || (password.length() > 20)) {
				messages.add("パスワードは6文字以上20文字以下です");
			}
			if (!password.matches("^[!-~]+$")) {
				messages.add("パスワードは記号を含む全ての半角文字のみです");
			}

			if (StringUtils.isBlank(password_confirmation)) {
				messages.add("確認パスワードを入力してください");
			} else {
				messages.add("確認パスワードが一致しません");
			}
		} else {
			if (!StringUtils.isBlank(password_confirmation)) {
				messages.add("パスワードを入力してください");
			}
		}

		// 名称のエラーメッセージ
		if (StringUtils.isBlank(user_name) == true) {
			messages.add("名称を入力してください");
		} else {
			if (user_name.length() > 10) {
				messages.add("名称は10文字以下です");
			}
		}
		// 支店と役職の組み合わせ
		if (Integer.parseInt(branch_code) == 1 && Integer.parseInt(department_position_code) >= 3) {
			messages.add("支店と役職の組み合わせが不正です。");
		}
		if (Integer.parseInt(branch_code) >= 2 && Integer.parseInt(department_position_code) <= 2) {
			messages.add("支店と役職の組み合わせが不正です。");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isNumber(String num) {
		try {
			Integer.parseInt(num);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

}
