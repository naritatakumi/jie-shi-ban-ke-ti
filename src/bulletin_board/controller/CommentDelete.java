package bulletin_board.controller;

import static bulletin_board.utils.CloseableUtil.*;
import static bulletin_board.utils.DBUtil.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bulletin_board.beans.Comment;
import bulletin_board.exception.SQLRuntimeException;

@WebServlet(urlPatterns = { "/commentDelete" })
public class CommentDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

        Comment comment = new Comment();
        comment.setComment_id(Integer.parseInt(request.getParameter("comment_id")));

        Connection connection = null;
        try {
            connection = getConnection();

            commentDelete(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

        response.sendRedirect("./");
	}

	public void commentDelete(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE comment_id= ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getComment_id());
            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
