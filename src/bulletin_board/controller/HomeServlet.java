package bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.User;
import bulletin_board.beans.View;
import bulletin_board.beans.ViewComment;
import bulletin_board.service.CommentService;
import bulletin_board.service.ContributionService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		Date date = new Date();
		View view = new View();

		view.setFirstDate("2018-06-01 00:00:00");
		view.setFinalDate(date.toString());
		view.setSerchCategory("");

		if (!StringUtils.isBlank(request.getParameter("date1"))) {
			view.setFirstDate(request.getParameter("date1"));
		}
		if (!StringUtils.isBlank(request.getParameter("date2"))) {
			view.setFinalDate(request.getParameter("date2") + " 23:59:59");
		}
		if (!StringUtils.isBlank(request.getParameter("category"))) {
			view.setSerchCategory(request.getParameter("category"));
		}

		HttpSession session = ((HttpServletRequest) request).getSession();
		User authority = (User) session.getAttribute("loginUser");
		List<View> viewList = new ContributionService().getViewContribution(view);
		List<ViewComment> viewComment = new CommentService().getViewComment();

		view.setFinalDate(request.getParameter("date2"));

		if (viewList.isEmpty()) {
			List<String> messages = new ArrayList<String>();
			messages.add("表示できる投稿はありません");
			// session.setAttribute("errorMessages", messages);
			request.setAttribute("authoritys", authority);
			request.setAttribute("retrievalErrorMessages", messages);
			request.setAttribute("reView", view);
			request.getRequestDispatcher("/home.jsp").forward(request, response);
		}

		request.setAttribute("reView", view);
		request.setAttribute("authoritys", authority);
		request.setAttribute("views", viewList);
		request.setAttribute("viewComments", viewComment);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}

}
