package bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.Contribution;
import bulletin_board.beans.User;
import bulletin_board.service.ContributionService;

@WebServlet(urlPatterns = { "/contribution" })
public class ContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("contribution.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            Contribution contribution = new Contribution();
            contribution.setSubject(request.getParameter("subject"));
            contribution.setCategory(request.getParameter("category"));
            contribution.setText(request.getParameter("text"));

            User user = (User) session.getAttribute("loginUser");

            contribution.setUser_id(user.getUser_id());

            new ContributionService().register(contribution);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);

            Contribution reContribution = new Contribution();
            reContribution.setSubject(request.getParameter("subject"));
            reContribution.setCategory(request.getParameter("category"));
            reContribution.setText(request.getParameter("text"));

            request.setAttribute("reContributions", reContribution);

            request.getRequestDispatcher("contribution.jsp").forward(request, response);
        }
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
        String subject = request.getParameter("subject");
        String category = request.getParameter("category");
        String text = request.getParameter("text");

        // ログインIDのエラーメッセージ
        if (StringUtils.isBlank(subject) == true) {
            messages.add("件名を入力してください");
        } else {
        	if (subject.length() > 30) {
                messages.add("件名は30文字以下です");
            }
        }
        // パスワードのエラーメッセージ
        if (StringUtils.isBlank(category) == true) {
            messages.add("カテゴリーを入力してください");
        } else {
        	if (category.length() > 10) {
                messages.add("カテゴリーは10文字以下です");
            }
        }
        // 確認用パスワードのエラーメッセージ
        if (StringUtils.isBlank(text) == true) {
            messages.add("本文を入力してください");
        } else {
        	if (text.length() > 1000) {
                messages.add("本文は1000文字以下です");
            }
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
