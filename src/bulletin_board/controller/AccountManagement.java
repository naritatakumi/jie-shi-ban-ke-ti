package bulletin_board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulletin_board.beans.Management;
import bulletin_board.beans.User;
import bulletin_board.service.AccountService;
import bulletin_board.service.ManagementService;

@WebServlet(urlPatterns = { "/accountManagement" })
public class AccountManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws IOException, ServletException {

		User user = new User();
		user.setAccount_management(Integer.parseInt(request.getParameter("account")));
		user.setUser_id(Integer.parseInt(request.getParameter("user_id")));

		AccountService accountService = new AccountService();
		accountService.parameterChange(user);

		HttpSession session = ((HttpServletRequest)request).getSession();
		User login = (User) session.getAttribute("loginUser");
		List<Management> management = new ManagementService().getManagement();

		request.setAttribute("managements", management);
		request.setAttribute("logins", login);

		response.sendRedirect("management");

	}

}
