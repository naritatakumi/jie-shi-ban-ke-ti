package bulletin_board.controller;

import static bulletin_board.utils.CloseableUtil.*;
import static bulletin_board.utils.DBUtil.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bulletin_board.beans.Contribution;
import bulletin_board.exception.SQLRuntimeException;

@WebServlet(urlPatterns = { "/contributionDelete" })
public class ContributionDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

        Contribution contribution = new Contribution();
        contribution.setId(Integer.parseInt(request.getParameter("contribution_id")));

        Connection connection = null;
        try {
            connection = getConnection();

            delete(connection, contribution);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

        response.sendRedirect("./");
	}

	public void delete(Connection connection, Contribution contribution) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM contributions WHERE id= ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, contribution.getId());
            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
