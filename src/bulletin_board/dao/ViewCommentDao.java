package bulletin_board.dao;

import static bulletin_board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.ViewComment;
import bulletin_board.exception.SQLRuntimeException;

public class ViewCommentDao {

	public List<ViewComment> getViewComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.contribbutions_id as contribbutions_id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.comment_id as comment_id, ");
            sql.append("contributions.id as id, ");
            sql.append("users.user_name as user_name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN contributions ");
            sql.append("ON comments.contribbutions_id = contributions.id ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.user_id ");
            sql.append("ORDER BY created_date limit " + num);



            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<ViewComment> ret = toViewCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<ViewComment> toViewCommentList(ResultSet rs)
            throws SQLException {

        List<ViewComment> ret = new ArrayList<ViewComment>();
        try {
            while (rs.next()) {
                int user_id = rs.getInt("user_id");
                String text = rs.getString("text");
                int comment_id = rs.getInt("comment_id");
                int contribbutions_id = rs.getInt("contribbutions_id");
                String user_name = rs.getString("user_name");
                int id = rs.getInt("id");
                Timestamp created_date = rs.getTimestamp("created_date");

                ViewComment view = new ViewComment();
                view.setUser_id(user_id);
                view.setContribbutions_id(contribbutions_id);
                view.setId(id);
                view.setComment_id(comment_id);
                view.setText(text);
                view.setUser_name(user_name);
                view.setCreated_date(created_date);

                ret.add(view);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
