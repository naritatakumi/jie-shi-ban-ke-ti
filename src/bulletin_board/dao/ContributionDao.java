package bulletin_board.dao;

import static bulletin_board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bulletin_board.beans.Contribution;
import bulletin_board.exception.SQLRuntimeException;

public class ContributionDao {

	public void insert(Connection connection, Contribution contribution) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO contributions ( ");
            sql.append("subject");
            sql.append(", category");
            sql.append(", text");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(") VALUES (");
            sql.append("?"); // subject
            sql.append(", ?"); // category
            sql.append(", ?"); // text
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, contribution.getSubject());
            ps.setString(2, contribution.getCategory());
            ps.setString(3, contribution.getText());
            ps.setInt(4, contribution.getUser_id());
            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
