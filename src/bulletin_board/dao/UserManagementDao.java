package bulletin_board.dao;

import static bulletin_board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.Management;
import bulletin_board.exception.SQLRuntimeException;

public class UserManagementDao {

	public List<Management> getUserManagement(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.user_id as user_id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.user_name as user_name, ");
            sql.append("users.branch_code as branch_code, ");
            sql.append("branches.branch_name as branch_name, ");
            sql.append("users.department_position_code as department_position_code, ");
            sql.append("departments_positions.department_position_name as department_position_name, ");
            sql.append("users.account_management as account_management, ");
            sql.append("users.created_date as created_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_code = branches.branch_code ");
            sql.append("INNER JOIN departments_positions ");
            sql.append("ON users.department_position_code = departments_positions.department_position_code");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<Management> ret = toUserManagementList(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Management> toUserManagementList(ResultSet rs)
            throws SQLException {

        List<Management> ret = new ArrayList<Management>();
        try {
            while (rs.next()) {
            	int user_id = rs.getInt("user_id");
            	String login_id = rs.getString("login_id");
            	String user_name = rs.getString("user_name");
            	int branch_code = rs.getInt("branch_code");
            	String branch_name = rs.getString("branch_name");
            	int department_position_code = rs.getInt("department_position_code");
            	String department_position_name = rs.getString("department_position_name");
            	int account_management = rs.getInt("account_management");
            	Timestamp created_date = rs.getTimestamp("created_date");

            	Management management = new Management();
                management.setUser_id(user_id);
                management.setLogin_id(login_id);
                management.setUser_name(user_name);
                management.setBranch_code(branch_code);
                management.setBranch_name(branch_name);
                management.setDepartment_position_code(department_position_code);
                management.setDepartment_position_name(department_position_name);
                management.setAccount_management(account_management);
                management.setCreated_date(created_date);

                ret.add(management);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
