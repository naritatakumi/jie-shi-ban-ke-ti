package bulletin_board.dao;

import static bulletin_board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.View;
import bulletin_board.exception.SQLRuntimeException;

public class ViewContributionDao {

    private List<View> toViewList(ResultSet rs)
            throws SQLException {

        List<View> ret = new ArrayList<View>();
        try {
            while (rs.next()) {
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                int id = rs.getInt("id");
                String category = rs.getString("category");
                String user_id = rs.getString("user_id");
                String user_name = rs.getString("user_name");
                Timestamp created_date = rs.getTimestamp("created_date");

                View view = new View();
                view.setSubject(subject);
                view.setUser_id(user_id);
                view.setId(id);
                view.setCategory(category);
                view.setText(text);
                view.setUser_name(user_name);
                view.setCreated_date(created_date);

                ret.add(view);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public List<View> getView(Connection connection, View view, int num) {

        PreparedStatement ps = null;
        try {
//        	Date date = new Date();
//
//        	if (!StringUtils.isBlank(view.getFirstDate())) {
//        		view.setFirstDate("2018-06-01 00:00:00");
//    		}
//    		if (!StringUtils.isBlank(view.getFinalDate())) {
//    			view.setFinalDate(date.toString());
//    		}

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("contributions.subject as subject, ");
            sql.append("contributions.category as category, ");
            sql.append("contributions.id as id, ");
            sql.append("contributions.user_id as user_id, ");
            sql.append("contributions.text as text, ");
            sql.append("users.user_name as user_name, ");
            sql.append("contributions.created_date as created_date ");
            sql.append("FROM contributions ");
            sql.append("INNER JOIN users ");
            sql.append("ON contributions.user_id = users.user_id ");
            sql.append("WHERE contributions.created_date >= ? AND contributions.created_date <= ? ");
            sql.append("AND category LIKE ? ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, view.getFirstDate());
            ps.setString(2, view.getFinalDate());
            ps.setString(3, "%" + view.getSerchCategory() + "%");

            ResultSet rs = ps.executeQuery();

            List<View> ret = toViewList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
