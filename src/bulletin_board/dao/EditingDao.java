package bulletin_board.dao;

import static bulletin_board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.Branch;
import bulletin_board.beans.Position;
import bulletin_board.beans.User;
import bulletin_board.exception.SQLRuntimeException;

public class EditingDao {

	public List<Branch> getBranch(Connection connection) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM branches";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<Branch> ret = toBranchList(rs);

	        return ret;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {

	    List<Branch> ret = new ArrayList<Branch>();
	    try {
	        while (rs.next()) {
	            int branch_code = rs.getInt("branch_code");
	            String branch_name = rs.getString("branch_name");

	            Branch branch = new Branch();
	            branch.setBranch_code(branch_code);
	            branch.setBranch_name(branch_name);

	            ret.add(branch);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public void editing(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET ");
			sql.append("login_id = ?, ");
			sql.append("user_name = ?, ");
			sql.append("branch_code = ?, ");
			if (user.getPassword() != null) {
				sql.append("password = ?, ");
            }
			sql.append("department_position_code = ? ");
			sql.append("WHERE user_id = ?");


            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getUser_name());
            ps.setInt(3, user.getBranch_code());
            if (user.getPassword() != null) {
            	ps.setString(4, user.getPassword());
                ps.setInt(5, user.getDepartment_position_code());
                ps.setInt(6, user.getUser_id());
            } else {
            	ps.setInt(4, user.getDepartment_position_code());
                ps.setInt(5, user.getUser_id());
            }


            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Position> getPosition(Connection connection) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM departments_positions";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<Position> ret = toPositionList(rs);

	        return ret;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<Position> toPositionList(ResultSet rs) throws SQLException {

	    List<Position> ret = new ArrayList<Position>();
	    try {
	        while (rs.next()) {
	            int department_position_code = rs.getInt("department_position_code");
	            String department_position_name = rs.getString("department_position_name");

	            Position position = new Position();
	            position.setDepartment_position_code(department_position_code);
	            position.setDepartment_position_name(department_position_name);

	            ret.add(position);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

}
