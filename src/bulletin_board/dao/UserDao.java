package bulletin_board.dao;

import static bulletin_board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.User;
import bulletin_board.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", user_name");
            sql.append(", branch_code");
            sql.append(", department_position_code");
            sql.append(", account_management");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // user_name
            sql.append(", ?"); // branch_code
            sql.append(", ?"); // department_position_code
            sql.append(", ?"); // account_management
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getUser_name());
            ps.setInt(4, user.getBranch_code());
            ps.setInt(5, user.getDepartment_position_code());
            ps.setInt(6, user.getAccount_management());
            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getLoginUser(Connection connection, String login_id,
	        String password) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, login_id);
	        ps.setString(2, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int user_id = rs.getInt("user_id");
	            String login_id = rs.getString("login_id");
	            String password = rs.getString("password");
	            String user_name = rs.getString("user_name");
	            String branch_code = rs.getString("branch_code");
	            String department_position_code = rs.getString("department_position_code");
	            String account_management = rs.getString("account_management");
	            Timestamp created_date = rs.getTimestamp("created_date");
	            Timestamp updated_date = rs.getTimestamp("updated_date");

	            User user = new User();
	            user.setUser_id(user_id);
	            user.setLogin_id(login_id);
	            user.setPassword(password);
	            user.setUser_name(user_name);
	            user.setBranch_code(Integer.parseInt(branch_code));
	            user.setDepartment_position_code(Integer.parseInt(department_position_code));
	            user.setAccount_management(Integer.parseInt(account_management));
	            user.setCreated_date(created_date);
	            user.setUpdated_date(updated_date);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public List<User> getUser(Connection connection, User user) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE user_id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, user.getUser_id());

	        ResultSet rs = ps.executeQuery();
	        List<User> ret = toUserList(rs);

	        return ret;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public boolean getValid(Connection connection, String str, String valid) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE " + str + " = " + valid;

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        if (userList.isEmpty()) {
	            return false;
	        } else {
	            return true;
	        }

	    } catch (SQLException e) {
	    	throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public void editing(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET login_id = ?, password = ?, user_name = ?, branch_code = ?, department_position_code = ? WHERE user_id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getUser_name());
            ps.setInt(4, user.getBranch_code());
            ps.setInt(5, user.getDepartment_position_code());
            ps.setInt(6, user.getUser_id());

            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void changeAccount(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET account_management = ? WHERE user_id = ?");

            ps = connection.prepareStatement(sql.toString());

            if (user.getAccount_management() == 0) {
            	ps.setInt(1, 1);
                ps.setInt(2, user.getUser_id());
            } else if (user.getAccount_management() == 1) {
            	ps.setInt(1, 0);
                ps.setInt(2, user.getUser_id());
            }

            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
