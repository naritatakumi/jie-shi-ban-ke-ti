package bulletin_board.service;

import static bulletin_board.utils.CloseableUtil.*;
import static bulletin_board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletin_board.beans.Branch;
import bulletin_board.beans.Position;
import bulletin_board.beans.User;
import bulletin_board.dao.EditingDao;
import bulletin_board.dao.UserDao;
import bulletin_board.utils.CipherUtil;

public class EditingService {

	public List<Branch> getBranchName() {

        Connection connection = null;
        try {
            connection = getConnection();

            EditingDao branchDao = new EditingDao();
            List<Branch> ret = branchDao.getBranch(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public List<Position> getPositionName() {

        Connection connection = null;
        try {
            connection = getConnection();

            EditingDao branchDao = new EditingDao();
            List<Position> ret = branchDao.getPosition(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public User getUserEditing(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> ret = userDao.getUser(connection, user);

            commit(connection);

            return ret.get(0);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public boolean getUserValid(String valid) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();

            if (userDao.getValid(connection, "user_id", valid)) {
            	return true;
            } else {
            	return false;
            }

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public void edit(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if (user.getPassword() != null) {
            	String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);
            }

            EditingDao editingDao = new EditingDao();
            editingDao.editing(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
