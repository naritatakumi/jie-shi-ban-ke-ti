package bulletin_board.service;

import static bulletin_board.utils.CloseableUtil.*;
import static bulletin_board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletin_board.beans.Comment;
import bulletin_board.beans.ViewComment;
import bulletin_board.dao.CommentDao;
import bulletin_board.dao.ViewCommentDao;

public class CommentService {

	public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao messageDao = new CommentDao();
            messageDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	private static final int LIMIT_NUM = 1000;

	public List<ViewComment> getViewComment() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        ViewCommentDao viewCommentDao = new ViewCommentDao();
	        List<ViewComment> retComment = viewCommentDao.getViewComment(connection, LIMIT_NUM);

	        commit(connection);

	        return retComment;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

}
