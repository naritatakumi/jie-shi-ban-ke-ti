package bulletin_board.service;

import static bulletin_board.utils.CloseableUtil.*;
import static bulletin_board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletin_board.beans.Contribution;
import bulletin_board.beans.View;
import bulletin_board.dao.ContributionDao;
import bulletin_board.dao.ViewContributionDao;

public class ContributionService {

	public void register(Contribution contribution) {

        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contributionDao = new ContributionDao();
            contributionDao.insert(connection, contribution);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	private static final int LIMIT_NUM = 1000;

	public List<View> getViewContribution(View view) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        ViewContributionDao viewDao = new ViewContributionDao();
	        List<View> ret = viewDao.getView(connection, view, LIMIT_NUM);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

}
