package bulletin_board.service;

import static bulletin_board.utils.CloseableUtil.*;
import static bulletin_board.utils.DBUtil.*;

import java.sql.Connection;

import bulletin_board.beans.User;
import bulletin_board.dao.UserDao;

public class AccountService {

	public void parameterChange(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.changeAccount(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
