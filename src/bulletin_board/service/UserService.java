package bulletin_board.service;

import static bulletin_board.utils.CloseableUtil.*;
import static bulletin_board.utils.DBUtil.*;

import java.sql.Connection;

import bulletin_board.beans.User;
import bulletin_board.dao.UserDao;
import bulletin_board.utils.CipherUtil;

public class UserService {

	 public void register(User user) {


	     Connection connection = null;
	     try {
	         connection = getConnection();

	         String encPassword = CipherUtil.encrypt(user.getPassword());
	         user.setPassword(encPassword);

	         UserDao userDao = new UserDao();
	         userDao.insert(connection, user);

	         commit(connection);
	     } catch (RuntimeException e) {
	         rollback(connection);
	         throw e;
	     } catch (Error e) {
	         rollback(connection);
	         throw e;
	     } finally {
	         close(connection);
	     }
    }

	 public boolean getLoginIdValid(String valid) {

		 Connection connection = null;
	     try {
	         connection = getConnection();

	         UserDao userDao = new UserDao();

	         if (userDao.getValid(connection, "login_id", "'" + valid + "'")) {
	             return true;
	         } else {
	             return false;
	         }

	     } catch (RuntimeException e) {
	         rollback(connection);
	         throw e;
	     } catch (Error e) {
	         rollback(connection);
	         throw e;
	     } finally {
	         close(connection);
	     }
	}

}
