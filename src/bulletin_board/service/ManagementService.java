package bulletin_board.service;

import static bulletin_board.utils.CloseableUtil.*;
import static bulletin_board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletin_board.beans.Management;
import bulletin_board.dao.UserManagementDao;

public class ManagementService {

	private static final int LIMIT_NUM = 1000;

	public List<Management> getManagement() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserManagementDao userManagementDao = new UserManagementDao();
	        List<Management> ret = userManagementDao.getUserManagement(connection, LIMIT_NUM);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

}
