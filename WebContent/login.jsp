<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css" style="text/css">
<link rel="stylesheet" style="text/css" href="./css/common.css">
<link rel="stylesheet" style="text/css" href="./css/login.css">
<title>ログイン</title>
</head>
<body>
	<div class="login-padding">
		<div class="main-contents" align="center">
			<h2>ログイン</h2>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul style="list-style-type: none;">
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="box5">
				<form action="login" method="post">
					<br /> <label for="login_id">ログインID</label> <input name="login_id"
						id="login_id" value="${reLoginUser.login_id }"
						style="width: 150px;" /> <br /> <br> <label for="password">パスワード</label>
					<input name="password" type="password" id="password"
						style="width: 150px;" /> <br /> <br> <input type="submit"
						value="ログイン" /> <br />
				</form>
			</div>
		</div>
		<div id="footer">
			<a>Copyright(c)NaritaTakumi</a>
		</div>
	</div>
</body>
</html>