<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" style="text/css" href="./css/navi.css">
<link rel="stylesheet" style="text/css" href="./css/common.css">
<link rel="stylesheet" style="text/css" href="./css/signup.css">
<title>ユーザー新規登録</title>

</head>
<body>
	<div class="main-contents" align="center">
		<ul id="nav">
			<li><a href="management">ユーザー管理へ</a></li>
		</ul>
		<br>
		<p class="title">-- ユーザー新規登録 --</p>
		<hr>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul style="list-style-type: none;">
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" /></li>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="signup" method="post">
				<table class="signup">
					<tr>
						<td class="items"><a>ログインID</a></td>
						<td><input name="login_id" id="login_id"
							value="${reSignupUser.login_id }" /><a class="note">半角英数字で6～20文字</a></td>
					</tr>
					<tr>
						<td class="items"><a>パスワード</a></td>
						<td><input name="password" type="password" id="password" /><a
							class="note">記号を含む全ての半角文字で6～20文字</a></td>
					</tr>
					<tr>
						<td class="items"><a>パスワード(確認)</a></td>
						<td><input name="password_confirmation" type="password"
							id="password_confirmation" /></td>
					</tr>
					<tr>
						<td class="items"><a>名称</a></td>
						<td><input name="user_name" id="user_name"
							value="${reSignupUser.user_name }" /><a class="note">10文字以下</a></td>
					</tr>
					<tr>
						<td class="items"><a>支店</a></td>
						<td><select name="branch_code">
								<c:forEach items="${branchNames}" var="branchName">
									<option value="${branchName.branch_code}">${branchName.branch_name}</option>
								</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td class="items"><a>部署・役職</a></td>
						<td><select name="department_position_code">
								<c:forEach items="${positionNames}" var="positionName">
									<option value="${positionName.department_position_code}">${positionName.department_position_name}</option>
								</c:forEach>
						</select></td>
					</tr>
				</table>
				<br />
				<div class="input">
					<input type="submit" value="登録" class="signup" />
				</div>
			</form>
			<br>
		</div>
	</div>
	<div id="footer">
		<a>Copyright(c)NaritaTakumi</a>
	</div>
</body>
</html>