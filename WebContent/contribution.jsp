<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" style="text/css" href="./css/navi.css">
    <link rel="stylesheet" style="text/css" href="./css/common.css">
    <link rel="stylesheet" style="text/css" href="./css/contribution.css">
    <title>新規投稿</title>

    <script>
	    function check () {
	        var flag = confirm ("入力内容を削除してもよろしいですか？\n\n削除したくない場合は[キャンセル]ボタンを押して下さい");
	        return flag;
	    }

	    function hoge() {
	    	document.getElementById("subject").value="";
	    	document.getElementById("category").value="";
	    	document.getElementById("text").value="";
	    }
	</script>

    </head>
    <body>
    <div id="wrapper">
    	<div class="main-contents" align="center">
	    	<ul id="nav">
	    		<li><a href="./">HOME</a></li>
	    	</ul><br>

	    	<p class="title" align="center">-- 新規投稿 --</p>
    		<c:if test="${ not empty errorMessages }">
    			<div class="errorMessages">
	                <ul style="list-style-type:none;">
	                    <c:forEach items="${errorMessages}" var="message">
	                        <li><c:out value="${message}" />
	                    </c:forEach>
	                </ul>
	            </div>
	            <c:remove var="errorMessages" scope="session" />
    		</c:if>

	    	<form method="post" action="contribution">
		    	<table class="contribution">
			    	<tr>
				    	<td class="items">件名(30文字以下)</td>
				    	<td><input type="text" name="subject" size="50" id="subject" value="${reContributions.subject }"></td>
			    	</tr>
			    	<tr>
				    	<td class="items">カテゴリー(10文字以下)</td>
				    	<td><input type="text" name="category" size="50" id="category" value="${reContributions.category }"></td>
			    	</tr>
			    	<tr>
				    	<td class="items">本文(1000文字以下)</td>
				    	<td><textarea name="text" rows="20" cols="50" id="text">${reContributions.text}</textarea></td>
			    	</tr>
		    	</table><br>
		    	<div class="input"><input type="submit" value="投稿" id="contribution" >
		    	<input type="button" id="clear" value="リセット" onClick="check(), hoge()" id="reset" /></div>
	    	</form>
    	</div>
    	<br>
    	<div id="footer">
        	<a>Copyright(c)NaritaTakumi</a>
        </div>
    </div>
    </body>
</html>