<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" style="text/css" href="./css/common.css">
<link rel="stylesheet" style="text/css" href="./css/navi.css">
<link rel="stylesheet" style="text/css" href="./css/editing.css">
<title>編集</title>

</head>
<body>
	<div class="main-contents">

		<ul id="nav">
			<li><a href="management">ユーザー管理へ</a></li>
		</ul>
		<br>
		<p class="title">-- ユーザー編集 --</p>
		<hr>

		<div align="center">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul style="list-style-type: none;">
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="editing" method="post">
				<table class="editing">
					<tr>
						<td class="items">ログインID</td>
						<td><input name="login_id" value="${editings.login_id }"
							id="login_id" /><a class="note">半角英数字で6～20文字</a></td>
					</tr>
					<tr>
						<td class="items">パスワード</td>
						<td><input name="password" type="password" id="password" /><a
							class="note">記号を含む全ての半角文字で6～20文字</a></td>
					</tr>
					<tr>
						<td class="items">パスワード(確認)</td>
						<td><input name="password_confirmation" type="password"
							id="password_confirmation" /></td>
					</tr>
					<tr>
						<td class="items">名称</td>
						<td><input name="user_name" value="${editings.user_name}"
							id="user_name" /><a class="note">10文字以下</a></td>
					</tr>
					<tr>
						<td class="items">支店</td>
						<td><select name="branch_code">
								<c:choose>
									<c:when test="${loginUser.user_id == editings.user_id}">
										<c:forEach items="${branchNames}" var="branchName">
											<c:if
												test="${branchName.branch_code == loginUser.branch_code}">
												<option value="${branchName.branch_code}">${branchName.branch_name}</option>
											</c:if>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach items="${branchNames}" var="branchName">
											<c:choose>
												<c:when
													test="${branchName.branch_code == editings.branch_code}">
													<option value="${branchName.branch_code}" selected>${branchName.branch_name}</option>
												</c:when>
												<c:otherwise>
													<option value="${branchName.branch_code}">${branchName.branch_name}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:otherwise>
								</c:choose>
						</select></td>
					</tr>
					<tr>
						<td class="items">部署・役職</td>
						<td><select name="department_position_code">
								<c:choose>
									<c:when test="${loginUser.user_id == editings.user_id}">
										<c:forEach items="${positionNames}" var="positionName">
											<c:if
												test="${positionName.department_position_code == loginUser.department_position_code}">
												<option value="${positionName.department_position_code}"
													selected>${positionName.department_position_name}</option>
											</c:if>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<c:forEach items="${positionNames}" var="positionName">
											<c:choose>
												<c:when
													test="${positionName.department_position_code == editings.department_position_code }">
													<option value="${positionName.department_position_code}"
														selected>${positionName.department_position_name}</option>
												</c:when>
												<c:otherwise>
													<option value="${positionName.department_position_code}">${positionName.department_position_name}</option>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:otherwise>
								</c:choose>
						</select></td>
					</tr>
				</table>
				<input type="hidden" name="user_id" value="${editings.user_id }" />
				<br />
				<div id="input">
					<input type="submit" value="編集" class="edit" />
				</div>
			</form>
			<br>
		</div>
	</div>
	<div id="footer">
		<a>Copyright(c)NaritaTakumi</a>
	</div>
</body>
</html>