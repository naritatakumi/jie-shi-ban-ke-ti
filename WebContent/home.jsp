<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="./css/style.css" style="text/css">
<link rel="stylesheet" style="text/css" href="./css/navi.css">
<link rel="stylesheet" style="text/css" href="./css/common.css">
<title>掲示板システム</title>

<script>
	function check(param) {
		var flag = confirm("この" + param
				+ "を削除してもよろしいですか？\n\n削除したくない場合は[キャンセル]ボタンを押して下さい");
		return flag;
	}

	function hoge() {
		document.getElementById("category").value = "";
		document.getElementById("date1").value = "";
		document.getElementById("date2").value = "";
	}
</script>

<!-- <script> -->
<!--  	history.forward(); -->
<!-- </script> -->

</head>
<body>
	<div id="container">
		<div id="header-fixed">
			<div id="header-bk">
				<ul id="nav">
					<li><a href="./">HOME</a></li>
					<li><a href="contribution">新規投稿</a></li>
					<c:if test="${authoritys.department_position_code == 1}">
						<li><a href="management">ユーザー管理</a></li>
					</c:if>
					<li><a href="logout" id="logout">ログアウト</a>
				</ul>
				<br>

				<hr>
				<div class="retrieval">
					<form method="get" action="./">
						<table>
							<tr>
								<td><a>カテゴリー検索</a></td>
								<td><input type="text" name="category" id="category"
									value="${reView.serchCategory}"></td>
							</tr>
							<tr>
								<td><a>日付検索</a></td>
								<td><input type="date" name="date1" id="date1"
									value="${reView.firstDate}"> ～ <input type="date"
									name="date2" id="date2" value="${reView.finalDate}"></td>
								<td><input type="submit" value="検索"> <input
									type="button" id="clear" value="リセット" onClick="hoge()" /></td>
							</tr>
						</table>
					</form>
				</div>
				<hr>
			</div>
		</div>

		<div id="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul style="list-style-type: none;">
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<c:if
				test="${ not empty retrievalErrorMessages and empty errorMessages}">
				<div class="errorMessages">
					<ul style="list-style-type: none;">
						<c:forEach items="${retrievalErrorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="main">
				<div class="contributions">
					<c:forEach items="${views}" var="view">
						<div class="contribution">
							<div id="subject">
								<c:out value="${view.subject}" />
							</div>
							<div class="user_name">
								投稿者:
								<c:out value="${view.user_name}" />
							</div>
							<hr>
							<c:forEach var="text"
								items="${fn:split(view.text, '
		                   		')}">
								<div class="text">
									<c:out value="${text}" />
								</div>
							</c:forEach>
							<div class="category">
								カテゴリー:
								<c:out value="${view.category}" />
							</div>
							<div class="date">
								<fmt:formatDate value="${view.created_date}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<c:if test="${loginUser.user_id == view.user_id }">
								<form action="contributionDelete" method="post">
									<input type="hidden" name="contribution_id" value="${view.id }" />
									<div align="right">
										<input type="submit" value="投稿削除" onClick="return check('投稿')"
											id="delete">
									</div>
								</form>
							</c:if>
						</div>

						<div class="comments">
							<c:forEach items="${viewComments}" var="viewComment">
								<c:if test="${view.id == viewComment.contribbutions_id}">
									<div class="comment">
										<div class="user_name">
											<c:out value="${viewComment.user_name}" />
										</div>
										<hr>
										<c:forEach var="text"
											items="${fn:split(viewComment.text, '
							                ')}">
											<div class="text">
												<c:out value="${text}" />
											</div>
										</c:forEach>
										<div class="date">
											<fmt:formatDate value="${viewComment.created_date}"
												pattern="yyyy/MM/dd HH:mm:ss" />
										</div>

										<c:if test="${viewComment.user_id == loginUser.user_id}">
											<form action="commentDelete" method="post">
												<input type="hidden" name="comment_id"
													value="${viewComment.comment_id }" />
												<div align="right">
													<input type="submit" value="コメント削除"
														onClick="return check('コメント')" id="delete">
												</div>
											</form>
										</c:if>
									</div>
								</c:if>
							</c:forEach>
						</div>

						<br>

						<div class="form-area">
							<form action="newComment" method="post">
								コメント欄<br />
								<c:choose>
									<c:when test="${reComments.id == view.id}">
										<textarea name="text" cols="100" rows="5" class="comment-box">${reComments.text}</textarea>
									</c:when>
									<c:otherwise>
										<textarea name="text" cols="100" rows="5" class="comment-box"></textarea>
									</c:otherwise>
								</c:choose>
								<br /> <input type="submit" value="コメント">（500文字まで） <input
									type="hidden" name="id" value="${view.id}">
							</form>
							<br>
						</div>
						<hr id="separate">

					</c:forEach>
					<hr>
				</div>
			</div>
		</div>
		<div id="footer">
			<a>Copyright(c)NaritaTakumi</a>
		</div>
	</div>
</body>
</html>