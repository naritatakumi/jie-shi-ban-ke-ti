<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" style="text/css" href="./css/management.css">
<link rel="stylesheet" style="text/css" href="./css/navi.css">
<link rel="stylesheet" style="text/css" href="./css/common.css">
<title>ユーザー管理</title>

<script>
	function check(name, param) {
		var flag = confirm(name + "さんのアカウントを" + param + "させてもよろしいですか？\n\n"
				+ param + "させたくない場合は[キャンセル]ボタンを押して下さい");
		return flag;
	}
</script>

</head>
<body>
	<div id="container">
		<ul id="nav">
			<li><a href="./">HOME</a></li>
			<li><a href="signup">ユーザー新規登録</a></li>
		</ul>
		<br>

		<p class="title" align="center">-- ユーザー管理 --</p>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages" align="center">
				<ul style="list-style-type: none;">
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div id="main">
			<table class="management">
				<thead>
					<tr>
						<th>ログインID</th>
						<th>名称</th>
						<th>支店</th>
						<th>部署・役職</th>
						<th>アカウント</th>
						<th>ユーザー編集</th>
					</tr>
				</thead>

				<tbody>
					<c:forEach items="${managements}" var="management">
						<tr>
							<td><c:out value="${management.login_id}" /></td>
							<td><c:out value="${management.user_name}" /></td>
							<td><c:out value="${management.branch_name}" /></td>
							<td><c:out value="${management.department_position_name}" /></td>
							<td class="account">
								<form action="accountManagement" method="post">
									<input type="hidden" name="account"
										value="${management.account_management}"> <input
										type="hidden" name="user_id" value="${management.user_id}">
									<c:choose>
										<c:when test="${management.user_id == logins.user_id }">
											<a>ログイン中</a>
										</c:when>
										<c:when test="${management.user_id != logins.user_id }">
											<c:choose>
												<c:when test="${management.account_management == 0}">
													<input type="submit" value="停止"
														onClick="return check('${management.user_name}', '停止')">
												</c:when>
												<c:when test="${management.account_management == 1}">
													<input type="submit" value="復活"
														onClick="return check('${management.user_name}', '復活')">
												</c:when>
											</c:choose>
										</c:when>
									</c:choose>
								</form>
							</td>

							<td class="edit">
								<form action="editing" method="get">
									<a href="editing?user_id=${management.user_id }">編集</a>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<br>
		<div id="footer">
			<a>Copyright(c)NaritaTakumi</a>
		</div>
	</div>
</body>
</html>